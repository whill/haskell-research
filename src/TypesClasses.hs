--Remove non-uppercase letters with explicit type declarations
removeNonUppercaseLetters :: [Char] -> [Char]
removeNonUppercaseLetters a = [x | x <-a, x `elem` ['A'..'Z']]

--Type definition with several parameters
sum3Numbers :: Int -> Int -> Int -> Int
sum3Numbers a b c = a+b+c

--For bigger numbers Integer can be used in place of Int
bigFactorial' :: Integer -> Integer
bigFactorial' number = product [1..number]

--Float
circumference :: Float -> Float
circumference radius = 2 * pi * radius

--Double
circumference' :: Double -> Double
circumference' radius = 2 * pi * radius

--Typeclasses
{--
:t (==)
   (==) :: Eq a => a -> a -> Bool
      Function composed of only special chars (=,+,-,*,/,^,>,<) is infix by defualt
      Can be passed to another function or in prefix notation by wrapping it in parentheses.
      (==) is a Function 'Eq' that specifies a class constraint via the => symbol.
      Which means the type of the two values being compared must be a member of the Eq class.
:t head
   head :: [a] -> a
:t fst
   fst :: (a,b) -> a
   :t (>)
   (>) :: Ord a => a -> a -> Bool

compareStrings "ab" "ac"
   True
--}
compareStringsLT :: String -> String -> Bool
compareStringsLT a b = a < b

{--
compareStringsInfixCompare "ab" "ac"
   LT
compareStringsInfixCompare "azb" "ac"
   GT
--}
compareStringsInfixCompare :: String -> String -> Ordering
compareStringsInfixCompare a b = a `compare` b

--String representation of a type that's a memeber of the Show typeclass.
{--
The types that have come up so far are members of the Show typeclass, so we can "show" them.
show 123
   "123"
show True
   "True"
--}
showMeTheString :: Show a => a -> String
showMeTheString a = show a

--Reverse of Show (kind of).
{--
The types that are members of the Read typeclass can be "read" (seems like very specific type inferencing).
read "123"
   123
read "123" + 333
   456
--}
readItToMe a = read a
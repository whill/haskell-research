--Tuples
returnNameAndAge fName lName age = (lName, fName, age)

--Zip lists into tuple pairs
{--
zipLists [1..] ["car","frog","dog"]
   [(1,"car"),(2,"frog"),(3,"dog")]
zipLists [1,2,3] [7,8,9]
   [(1,7),(2,8),(3,9)]
--}
zipLists a b = zip a b

--List comprehension with multiple generators to form iterative tuples
{--
[(a,b,c) | a <- [1..10], b <- [1..10], c <- [1..10]]
This is similar to the last example in the ListComprehensions module, outside to inside
[(1,1,1),(1,1,2),(1,1,3),(1,1,4),(1,1,5),(1,1,6),(1,1,7),(1,1,8),(1,1,9),(1,1,10),(1,2,1),(1,2,2)
..
(10,9,9),(10,9,10),(10,10,1),(10,10,2),(10,10,3),(10,10,4),(10,10,5),(10,10,6),(10,10,7),(10,10,8),(10,10,9),(10,10,10)]
--}

--Using List comprehension predicates to return triples (a way of referring to tuples like: (1,2,3) vs a pair like: (1,2)).
{--
This is a useful example of the preceeding technique. In order to find a triple that satifies the right triangle rule.
The end result sets the precedent i.e. iteration cap for a and b. Then b caps a and if the combination satisfies
a^2 + b^2 == c^2, the combination can be added to the list of tuples to be returned.
So..1,1,1 is bad, 2,1,1 is bad, 2,1,2 is bad... 5,4,2 is bad, c=5,b=4,a=3 is good 25 = 16+9
howAboutSomeTriangles 10
   [(3,4,5),(6,8,10)]
--}
howAboutSomeTriangles n = [(a,b,c) | c <- [1..n], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2]
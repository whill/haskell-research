--Return double the value of each number in some list, so long as it's <= 10.
doubleIt someList = [x*2 | x <- someList, x <= 10]

--Return double the value of each element. In this case, 1 through 10.
justOneToTenDoubled = [x*2 | x <- [1..10]]

--Return double the value of each element in the list with a step of 2 from 2 to 10 and when element * 2 is >= 12.
doubledTwoStepToTenGT12 = [x*2 | x <- [2,4..10], x*2 >= 12]

--Return triple value of each element from 50 to 200, in even increments of 2 but only when remainder of /7 == 3.
infixModulusRemainder3 = [x*3 | x <- [50,52..200], x `mod` 7 == 3]
prefixModulusRemainder3 = [x*3 | x <- [50,52..200], mod x 7 == 3]

{--
boomOrBang [7,14..210]
   ["BOOM","BOOM","BANG!","BOOM","BOOM","BANG!","BOOM","BOOM","BANG!","BOOM","BOOM","BANG!","BOOM","BOOM","BANG!","BOOM","BOOM",
 "BANG!","BOOM","BOOM","BANG!","BOOM","BOOM","BANG!","BOOM","BOOM","BANG!","BOOM","BOOM","BANG!"]
--}
boomOrBang someList = [if x `mod` 3 == 0 then "BANG!" else "BOOM" | x <- someList]

--Return 7,14,28...91 without 21 and 49.
notAParticularNumberWithComprehensionAndGeneration = [subSet | subSet <- [7,14..91], subSet /= 21, subSet /= 49]

--Return the length of some list. Recursive sum.
getTheLengthOfAList someList = sum [1 | _ <- someList]

--Return funny strings! let nouns = ["boat","car","bunny"] and let adjectives = ["fast", "cool", "silly", "little"]
{--
sillyMixUp ["boat","car","bunny"] ["fast", "cool", "silly", "little"]
   ["fast boat","fast car","fast bunny","cool boat","cool car","cool bunny","silly boat","silly car","silly bunny","little boat",
 "little car","little bunny"]
--}
sillyMixUp nouns adjectives = [adjective ++ " " ++ noun | adjective <- adjectives, noun <- nouns]

--Return uppercase characters in a string. Comprehensions work since strings are lists of chars.
{--
tallLettersOnly "Winfield G Hill"
   "WGH"
--}
tallLettersOnly stringOfChars = [tallLetters | tallLetters <- stringOfChars, tallLetters `elem` ['A'..'Z']]

--Return lowercase characters in a string.
{--
smallLettersOnly "Winfield G Hill"
   "infieldill"
--}
smallLettersOnly stringOfChars = [smallLetters | smallLetters <- stringOfChars, smallLetters `elem` ['a'..'z']]

--Return the even elements in lists of a list i.e. [[1,2,3,4,5,6][7,8,9,10][11,12,13]]
--Outer loop and inner loop -> for lists in list -> for item in list
--  |   |   |   |   |   |   Inner Loop  |   |   |   |   Outer Loop  |   |   |   |   |   |
justTheEvensPlease = [[x | x <- xs, even x] | xs <- [[1,2,3,4,5,6],[7,8,9,10],[11,12,13]]]
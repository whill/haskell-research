doubleIt x = x * 2

doubleUs x y = x*2 + y*2

doubleSmallNumber x = if x < 100
                        then x*2
                        else x

it'sARealFunctionName = "The apostrophe is usually used for strict/eager functions"

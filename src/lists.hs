--Retun some char or number or string in a list
returnAList x = [x]

--Return a char, num, string, lists or tupples within a list
concatListsInAList x y = [x] ++ [y]

--Return the concatenation of two lists
concatLists x y = x ++ y

--Join a Char to an Existing Sting at the head of the list
concatCharToHead c = c:" is a..."

--List Syntatic Sugar i.e. [1,2,3,4,5,...] is essentially this ->
giveMeNumbers a b c d e = a:b:c:d:e:[]

--Return the 3rd element in a list
returnThe3rd x = x !! 2

--isFirstListBigger [1,2,5] [1,2,4] -> True
--isFirstListBigger [1,2,3] [1,2,4] -> False
isFirstListBigger x y = x > y

--areListsEqual [1,2,3] [1,2,3] -> True
--areListsEqual [1,2,3] [1,2,3,4] -> False
areListsEqual x y = x == y

--Return the first element in a list
returnTheHeadofAList x = head x

--Return the tal of a list
returnTheTailofAList x = tail x
returnTheLastElementinAList x = last x
returnAllButTheLastElementInAList x = init x

--Remove n elements from front of the list
dropTheEles x list = drop x list

--Return the length of a list
getListLength list = length list

--Return if list is null i.e. empty
isListNull x = null x

--Return a list in reverse
reverseAList list = reverse list

--Return the first n elements from a list
takeFromAList n list = take n list

--Return the smallest
getSmallestElementInList list = minimum list

--Return the largest
getBiggestElementInList list = maximum list

--Return the sum of the eles in a list
sumElementsInAList list = sum list

--Return the product of elements in a list
productOfElementsInAList list = product list

--Return if a thing exists in a list
doesThingExistInTheList thing list = elem thing list

--Return if a thing exists in a list with infix notation
doesThingInfixExistInTheList thing list = thing `elem` list

--Return a composed list based on a range
{--
generateListBasedOnARange 1 20
   [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

generateListBasedOnARange  'a' 'z'
   "abcdefghijklmnopqrstuvwxyz"
--}
generateListBasedOnARange a z = [a..z]

--Return a list based on a range and a step
{--
generateListBasedOnARangeAndStep 8 16 24
   [8,16,24]
generateListBasedOnARangeAndStep 4 8 20
   [4,8,12,16,20]
generateListBasedOnARangeAndStep 2 8 20
   [2,8,14,20]
generateListBasedOnARangeAndStep 2 4 20
   [2,4,6,8,10,12,14,16,18,20]
--}
generateListBasedOnARangeAndStep s a z = [s,a..z]

--Return some repeated pattern within a list using cycle and a trimmer i.e. take
{--
takeFromPattern 11 "LOL "
   "LOL LOL LOL"
--}
takeFromPattern thisAmount pattern = take thisAmount (cycle pattern)

--Return some repeated element within a list using repeat and a trimmer i.e. take
{--
takeFromPatternRepeat 5 5
   [5,5,5,5,5]
--}
takeFromPatternRepeat thisAmount singleElement = take thisAmount (repeat singleElement)